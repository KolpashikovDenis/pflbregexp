import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    static List<String> lines;
    static List<String> listMatches;

    public static void main(String[] args) {
        if(args.length < 3){
            System.out.println(" Usage: 3 args:");
            System.out.println("   <RegExp> <path_to_log> <name_of_new_file>");
            return;
        }else{
            try{
                lines = Files.readAllLines(Paths.get(args[1]), StandardCharsets.ISO_8859_1);
                File file = new File(args[2]);
                file.createNewFile();
                file.setWritable(true);
                OutputStream fOutput = new FileOutputStream(file);
                OutputStreamWriter fWriter = new OutputStreamWriter(fOutput, "ISO-8859-1");

                listMatches = new ArrayList<String>();
                Pattern p = Pattern.compile(args[0]);

                for(String line: lines){
                    Matcher m = p.matcher(line);
                    if(m.find()){
                        listMatches.add(line);
                        fWriter.write(line+"\r\n");
                        fWriter.flush();
                    }
                }
                System.out.println("Done.");
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
